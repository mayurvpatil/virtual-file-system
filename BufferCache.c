
//This program contains data structure of buffer cache , creation on buffer cache and getBlk system call
//remining : deallocation

#include<stdio.h>
#include<stdlib.h>
#define BLK_SIZE 256
//Used free and Busy as status
//busy buffer are not getting free , need to add some module for that 


void create_cache(int); 
struct Buffer_Cache* Remove_First_Buffer_FL();
void Remove_Buffer_From_FL(struct Buffer_Cache*);
struct Buffer_Cache* Check_HQ(int);
void Display();

struct Buffer_Cache* pHashIndex[4]={NULL,NULL,NULL,NULL}; //Array of pointers 
struct Buffer_Cache* pFL=NULL ; // Free List head 

struct Buffer_Cache
{	
	
	int iBlk ;
	int iStatus ;  // 0 : Free , 1 :  BUSY
	int * pData ;
	struct Buffer_Cache* pHQNext; 
	struct Buffer_Cache* pHQPrev ;
	struct Buffer_Cache* pFLNext ;
	struct Buffer_Cache* pFLPrev ;
};

int main()
{

	int n=0,blk=0;

	struct Buffer_Cache* pTemp=NULL;
	printf("Enter the Nuber of buffer you want : ");
	scanf("%d" , &n );
	Create_Cache(n);
	printf("\nPress control + z to exit \n");
		
	while(1)
	{
		printf("\n\nEnter block  Number :");
		scanf("%d",&blk);
		if(pTemp = Check_HQ(blk))
		{	//printf("%d",pTemp);	
			if(pTemp->iStatus == 0) //free 
			{	
				printf(" \nIN 1\n");
				pTemp->iStatus = 1;
				Remove_Buffer_From_FL(pTemp);
				printf("\nblock allocated is %d \n", pTemp);

			}
			else
			{	
				// wait
				printf("\nSleep :Event Buffer become free \n");
				
			}

		}
		else 
		{	
			
			if(pFL==NULL)
			{	
				printf("\nSleep : Event Buffer become free \n");
				continue;
			}

			pTemp = Remove_First_Buffer_FL();

			// delayed write 

			Remove_Buffer_HQ(pTemp);
			pTemp->iBlk = blk ;
			Add_Buffer_HQ(pTemp,blk);
			pTemp->iStatus = 1 ;

			printf("\nblock allocated is %d\n", pTemp);			

		}
		Display();

	}
}

void Add_Buffer_HQ(struct Buffer_Cache* pBuff,int blk_no)
{
	int iHFValue = blk_no%4;
	printf("%d",iHFValue);
	struct Buffer_Cache* pTemp = pHashIndex[iHFValue];

	if(pTemp==NULL)
	{    
		pTemp=pBuff; 
		pBuff->pHQNext =NULL ;
		pBuff->pHQPrev = pTemp;
		pHashIndex[iHFValue] = pBuff ;

	}

	else
	{
		pBuff->pHQNext=pTemp;
		//pTemp->pHQNext->pHQPrev=pBuff;
		pTemp->pHQPrev=pBuff;
		pBuff->pHQPrev=pBuff;
		pHashIndex[iHFValue]=pBuff;
	}
}

void Remove_Buffer_HQ(struct Buffer_Cache* pTemp)
{
	int index = (pTemp->iBlk)%4 ;
	struct Buffer_Cache* pNext ;
	struct Buffer_Cache* pPrev;

	if(pHashIndex[index] == pTemp->pHQPrev)
	{
		pHashIndex[index] = pTemp->pHQNext ;
		if(pTemp -> pHQNext != NULL)		
			pTemp->pHQNext->pHQPrev = pHashIndex[index];	
	}
	else 
	{
	pNext = pTemp->pHQNext ;
	pPrev = pTemp->pHQPrev ;

	pNext->pHQPrev = pPrev; 
	pPrev->pHQNext = pNext;	
	}

}

struct Buffer_Cache* Remove_First_Buffer_FL()
{	
	struct Buffer_Cache* pTemp = pFL ;
	struct Buffer_Cache* pNext ;
	if(pFL ->pFLNext != NULL)
	{
	
		pNext = pTemp->pFLNext;

		pFL = pNext;
		pNext->pFLPrev = pFL;
	}
	else 
	{
		pFL = NULL ;
	}

return pTemp ;
}

void Remove_Buffer_From_FL(struct Buffer_Cache* pTemp)
{

	struct Buffer_Cache* pPrev ;
	struct Buffer_Cache* pNext ;
	if(pFL!= NULL)
	{
		pPrev = pTemp -> pFLPrev;
		pNext = pTemp -> pFLNext;

		pPrev->pFLNext = pNext ;
		pNext->pFLPrev = pPrev ;

		pTemp->pFLNext = NULL;
		pTemp->pFLPrev = NULL;
	}
}

struct Buffer_Cache* Check_HQ(int blk_no)
{
	int HFValue = blk_no%4;
	//printf("\nHFValue = %d ",HFValue);
	struct Buffer_Cache* pTemp = pHashIndex[HFValue];
	if (pTemp!=NULL){
		while(pTemp != NULL)
		{		

			if(pTemp->iBlk == blk_no)
			{	
				printf("\nfound in HQ\n");
				return pTemp;
			}
			pTemp = pTemp->pHQNext;

		}
	}
	printf("\nNot found in HQ,null return\n");
	return NULL;
}

void Create_Cache(int n)
{
	int i;
	struct Buffer_Cache* pTemp;
	struct Buffer_Cache* pLast;
	pTemp = (struct Buffer_Cache *)malloc(sizeof(struct Buffer_Cache));
	pTemp->pData = (int *) malloc (BLK_SIZE);
	pHashIndex[0] = pTemp ;
	pTemp->iBlk = NULL;
	pTemp->pData=(int*)malloc(256);
	pTemp->pHQPrev = pHashIndex[0] ;
	pTemp->pFLPrev =pFL ;
	pFL = pTemp ;
	pTemp->iStatus = 0;
	pTemp->pHQNext =NULL;
	pTemp->pFLNext = NULL;
	pLast = pTemp ;

	for(i=1 ; i<n ; i++)
	{
		pTemp = (struct Buffer_Cache *)malloc(sizeof(struct Buffer_Cache));
		pTemp->pData = (int *) malloc (BLK_SIZE);		
		pTemp->iBlk = NULL;
		pTemp->pData=(int*)malloc(256);
		pTemp->pHQPrev =pLast;
		pTemp->pFLPrev = pLast ;
		pLast->pHQNext =pTemp;
		pLast->pFLNext = pTemp ;
		pTemp->iStatus = 0 ;
		pTemp->pHQNext = NULL;
		pTemp->pFLNext = NULL ;
		pLast = pTemp ;
		//printf("node : %d\n",i);
	} 
}

void Display()
{
	int i, j;
	struct Buffer_Cache* pTemp ;
	printf(" Hash Table is  : \n");

	for(i=0 ; i <4 ; i++)
	{	
		printf("\nHashQueue %d :\t" , i);
		if (pHashIndex[i] == NULL)	
		{
			printf(" --> NULL");
		}
		else
		{
			pTemp = pHashIndex[i];
			while(pTemp !=NULL)	
			{

				printf("--> %d/%s",pTemp->iBlk,(pTemp->iStatus == 0) ? "Free" : "Busy" );
				pTemp = pTemp->pHQNext;
			}
		}
	}

	printf(" \n\nFree List :");
	
	
	if(pFL == NULL)
	{	
		printf("--> NULL");
	}
	else
	{	
		pTemp = pFL ;
		while(pTemp != NULL)
		{
			printf("--> %d ",pTemp->iBlk);
			pTemp = pTemp->pFLNext;
		}
	}

}			

/*int i;
  struct Buffer_Cache* pTemp;
  printf("\n\na[0] = %u",pHashIndex[0]);
  printf("\npFL = %u\n\n",pFL);
  pTemp = pHashIndex[0] ;
  printf("in disp");
  for(i=0;i<n;i++)
  {
  printf("%u : Node : %u HQNext = %u , HQPre = %u , pFLNext = %u , pFLPre = %u\n\n",i,pTemp,pTemp->pHQNext , pTemp->pHQPrev ,pTemp->pFLNext, pTemp->pFLPrev );
  pTemp=pTemp->pHQNext;
  }
  printf("Free List\n\n ");
  pTemp = pFL;
  while(pTemp->pFLNext != NULL)
  {     
  printf("\t%u",pTemp);
  pTemp=pTemp->pFLNext;
  } 

 */

