# Virtual File System #

This project is created for Educational purpose for those who fall in love with UNIX File System.
The project is exact replica of Unix file system where we are implementing all file system data structures.
We are trying to create same structure virtually on Memory and our current goal is to create CUI base (Menu driven) Application.

Contributing
------------
Use any Linux System. 
Please refer to each project's style guidelines and guidelines for submitting patches and additions. In general, we follow the "fork-and-pull" Git workflow.

 1. **Fork** the repo on GitHub
 2. **Clone** the project to your own machine
 3. **Commit** changes to your own branch
 4. **Push** your work back up to your fork
 5. Submit a **Pull request** so that we can review your changes

NOTE: Be sure to merge the latest from "upstream" before making a pull request!

Please read [CONTRIBUTING.md](https://bitbucket.org/mayurvpatil/virtual-file-system/src/b1ecada11833d33dc0ff846e8d72fb035ff16ecc/CONTRIBUTING.md?at=master) for details on our code of conduct, and the process for submitting pull requests to us.

Project [Tasks List](https://bitbucket.org/mayurvpatil/virtual-file-system/src/9cbb113ab7fee729fe15ccc36b2edc0cf09de7bc/Tasks%20List.md?at=master&fileviewer=file-view-default)

Feel free to submite your suggestion and Issues [here](https://bitbucket.org/mayurvpatil/virtual-file-system/issues?status=new&status=open)


## Authors

* **Mayur Patil** - *Initial work* - 

See also the list of [contributors]() who participated in this project.