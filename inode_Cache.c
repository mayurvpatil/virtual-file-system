//INODE CACHE
#include<stdio.h>
#include<stdlib.h>


typedef unsigned short int MODE_T;
typedef unsigned int NLINK;
typedef unsigned short int INT16;
typedef unsigned int SIZE_T;
typedef unsigned int TIME_T;
typedef long long int INTLL;
typedef int* INT_P;


//Used free and Busy as status
//busy INode are not getting free , need to add some module for that 

void Create_INode_Cache(int n);


struct Incore_INode* Check_INode_Cache(int);

struct Incore_INode* iget(int);
struct Incore_INode* Remove_First_INode_FL();
void Remove_INode_From_FL(struct Incore_INode*);
void Remove_INode_HQ(struct Incore_INode*);
struct Incore_INode* Check_HQ(int);
void Add_INode_HQ(struct Incore_INode*,int);
void Display();

struct Incore_INode * pHashIndex[4]={NULL,NULL,NULL,NULL}; //Array of pointers 
struct Incore_INode * pFL=NULL ; // Free List head 

struct Incore_INode
{	

	MODE_T 	  mode ; 		// 3-bits for file type & 9 bits for permissions 
	INT16 	  uid ;			// user id
	NLINK 	  link ;		// Numbers of links  
	//INT16	 st_gid ;		// group id 
	SIZE_T	  size ;		// size in bytes for regular file 
	TIME_T 	 at_time;		// File access time 
	TIME_T	 mt_time;		// Modification time 
	TIME_T   ct_time;		// I-node change time 
	INTLL 	 Reserved;		// extra reserved byter to make inode in power of 2 

	//Additional parameters to inode cache 

	int iINode; 		// additional to incore inode
	int iStatus ;   		// 0 : UNLOCK  , 1 :  LOCK 
	// status : process is waiting to inode become unlock is not used.(Project is not multi threaded)
	// Point to other incore inode (link)
	INT_P  pTable_Of_Content[7];
	int    iRefrence_Count;
	struct Incore_INode * pHQNext; 
	struct Incore_INode * pHQPrev ;
	struct Incore_INode * pFLNext ;
	struct Incore_INode * pFLPrev ;
};

int main()
{

	int n=0,inode=0;
	struct Incore_INode* pTemp=NULL;
	
	printf("Enter the Nuber of INode you want : ");
	scanf("%d" , &n );
	Create_INode_Cache(n);
	printf("\nPress control + z to exit \n");
		
	//iget(INode);	
	
	while(1)
	{
		printf("\n\nEnter INODE  Number :");
 		scanf("%d",&inode);
		
		if(pTemp = Check_INode_Cache(inode))
		{	//printf("%d",pTemp);	
			if(pTemp->iStatus == 0) //free 
			{	
				pTemp->iStatus = 1;
				Remove_INode_From_FL(pTemp);
				pTemp->iRefrence_Count=1;				
				printf("\nInode allocated is %d \n", pTemp);
				//return pTemp;
			}
			else
			{	

				printf("\nSleep :Event INode become free \n");
				//continue
			}

		}
		else 
		{	

			if(pFL==NULL)
			{	
				printf("\n Error  \n");
				continue;
			}

			pTemp = Remove_First_INode_FL();

			// delayed write
		 

			Remove_INode_HQ(pTemp);
			pTemp->iINode = inode ;
			Add_INode_HQ(pTemp,inode);
			pTemp->iStatus = 1 ;
			pTemp->iRefrence_Count = 1;			
			//reset file ststem ,ie super block 
			//call bread
			printf("\nblock allocated is %d\n", pTemp);			
			//return pTemp;
		}
		Display();

	}



}
/*
struct Incore_INode* iget(int inode)
{	int INode;
	
}*/

void Add_INode_HQ(struct Incore_INode* pBuff,int INode_no)
{
	int iHFValue = INode_no%4;
	printf("%d",iHFValue);
	struct Incore_INode* pTemp = pHashIndex[iHFValue];

	if(pTemp==NULL)
	{    
		pTemp=pBuff; 
		pBuff->pHQNext =NULL ;
		pBuff->pHQPrev = pTemp;
		pHashIndex[iHFValue] = pBuff ;

	}

	else
	{
		pBuff->pHQNext=pTemp;
		//pTemp->pHQNext->pHQPrev=pBuff;
		pTemp->pHQPrev=pBuff;
		pBuff->pHQPrev=pBuff;
		pHashIndex[iHFValue]=pBuff;
	}
}

void Remove_INode_HQ(struct Incore_INode* pTemp)
{
	int index = (pTemp->iINode)%4;
	struct Incore_INode* pNext ;
	struct Incore_INode* pPrev;

	if(pHashIndex[index] == pTemp->pHQPrev)
	{
		pHashIndex[index] = pTemp->pHQNext ;
		if(pTemp -> pHQNext != NULL)
			pTemp->pHQNext->pHQPrev = pHashIndex[index];	
	}
	else 
	{
		pNext = pTemp->pHQNext ;
		pPrev = pTemp->pHQPrev ;

		pNext->pHQPrev = pPrev; 
		pPrev->pHQNext = pNext;	
	}

}

struct Incore_INode* Remove_First_INode_FL()
{	
	struct Incore_INode* pTemp = pFL ;
	struct Incore_INode* pNext ;
	if(pFL ->pFLNext != NULL)
	{
		pNext = pTemp->pFLNext;
		pFL = pNext;
		pNext->pFLPrev = pFL;
	}
	else 
	{
		pFL = NULL ;
	}

	return pTemp ;
}


void Remove_INode_From_FL(struct Incore_INode* pTemp)
{

	struct Incore_INode* pPrev ;
	struct Incore_INode* pNext ;
	if(pFL!= NULL)
	{
		pPrev = pTemp -> pFLPrev;
		pNext = pTemp -> pFLNext;
	
		pPrev->pFLNext = pNext ;
		pNext->pFLPrev = pPrev ;

		pTemp->pFLNext = NULL;
		pTemp->pFLPrev = NULL;
	}
}

struct Incore_INode* Check_INode_Cache(int INode_no)
{
	int HFValue = INode_no%4;
	//printf("\nHFValue = %d ",HFValue);
	struct Incore_INode* pTemp = pHashIndex[HFValue];
	if (pTemp!=NULL){
		while(pTemp != NULL)
		{		

			if(pTemp->iINode == INode_no)
			{	
				printf("\nfound in HQ\n");
				return pTemp;
			}
			pTemp = pTemp->pHQNext;

		}
	}
	printf("\nNot found in HQ,null return\n");
	return NULL;
}

void Create_INode_Cache(int n)
{
	int i;
	struct Incore_INode* pTemp;
	struct Incore_INode* pLast;
	pTemp = (struct Incore_INode *)malloc(sizeof(struct Incore_INode));
	//pTemp = 0;
	pHashIndex[0] =pTemp ;
	pTemp->iINode = NULL;
	pTemp->iStatus =0;
	pTemp->pHQPrev =pHashIndex[0] ;
	pTemp->pFLPrev =pFL ;
	pFL = pTemp ;
	pTemp->pHQNext =NULL;
	pTemp->pFLNext = NULL;
	pLast = pTemp ;

	for(i=1 ; i<n ; i++)
	{
		pTemp = (struct  Incore_INode *)malloc(sizeof(struct  Incore_INode));
		//pTemp = 0;
		pTemp->iStatus =0;
		pTemp->iINode =NULL;		
		pTemp->pHQPrev =pLast;
		pTemp->pFLPrev =pLast ;
		pLast->pHQNext =pTemp;
		pLast->pFLNext = pTemp ;
		pTemp->pHQNext =NULL;
		pTemp->pFLNext =NULL ;
		pLast = pTemp ;
		
	} 
}

void Display()
{
	int i, j;
	struct Incore_INode* pTemp ;
	printf(" Hash Table is  : \n");

	for(i=0 ; i <4 ; i++)
	{	
		printf("\nHashQueue %d :\t" , i);
		if (pHashIndex[i] == NULL)	
		{
			printf(" --> NULL");
		}
		else
		{
			pTemp = pHashIndex[i];
			while(pTemp !=NULL)	
			{

				printf("--> %d/%s",pTemp->iINode,(pTemp->iStatus == 0) ? "Free" : "Busy" );
				pTemp = pTemp->pHQNext;
			}
		}
	}

	printf(" \n\nFree List :");


	if(pFL == NULL)
	{	
		printf("--> NULL");
	}
	else
	{	
		pTemp = pFL ;
		while(pTemp != NULL)
		{
			printf("--> %d ",pTemp->iINode);
			pTemp = pTemp->pFLNext;
		}
	}

}			

/*int i;
  struct Incore_INode* pTemp;
  printf("\n\na[0] = %u",pHashIndex[0]);
  printf("\npFL = %u\n\n",pFL);
  pTemp = pHashIndex[0] ;
  printf("in disp");
  for(i=0;i<n;i++)
  {
  printf("%u : Node : %u HQNext = %u , HQPre = %u , pFLNext = %u , pFLPre = %u\n\n",i,pTemp,pTemp->pHQNext , pTemp->pHQPrev ,pTemp->pFLNext, pTemp->pFLPrev );
  pTemp=pTemp->pHQNext;
  }
  printf("Free List\n\n ");
  pTemp = pFL;
  while(pTemp->pFLNext != NULL)
  {     
  printf("\t%u",pTemp);
  pTemp=pTemp->pFLNext;
  } 

 */
#include<stdio.h>

