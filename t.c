#include<stdio.h>
#include<stdlib.h>

void create_cache(int);
struct Buffer* Remove_First_Buffer_FL();
void Remove_Buffer_From_FL(struct Buffer*);
struct Buffer* Check_HQ(int);
void disp(int);


struct Buffer* pHashIndex[4];
struct Buffer* pFL ;

struct Buffer
{
	int iBlk ;
	int iStatus ;  // 0 : Free , 1 :  BUSY
	//int * pData ;
	struct Buffer* pHQNext; 
	struct Buffer* pHQPrev ;
	struct Buffer* pFLNext ;
	struct Buffer* pFLPrev ;
};

int main()
{

	int n,blk;
	struct Buffer* pTemp ;
	printf("Enter the Nuber of buffer you want : ");
	scanf("%d" , &n );
	Create_Cache(n);
	disp(n);


}

void Create_Cache(int n)
{
	int i;
	struct Buffer* pTemp;
	struct Buffer* pLast;
	pTemp = (struct Buffer *)malloc(sizeof(struct Buffer));
	pHashIndex[0] = pTemp ;
	pTemp->iBlk = NULL;
	pTemp->pHQPrev = pHashIndex[0] ;
	pTemp->pFLPrev =pFL ;
	pFL = pTemp ;
	pTemp->iStatus = 0;
	pTemp->pHQNext =NULL;
	pTemp->pFLNext = NULL;
	pLast = pTemp ;

	for(i=1 ; i<n ; i++)
	{
		pTemp = (struct Buffer *)malloc(sizeof(struct Buffer));
		pTemp->iBlk = NULL;
		pTemp->pHQPrev =pLast;
		pTemp->pFLPrev = pLast ;
		pLast->pHQNext =pTemp;
		pLast->pFLNext = pTemp ;
		pTemp->iStatus = 0 ;
		pTemp->pHQNext = NULL;
		pTemp->pFLNext = NULL ;
		pLast = pTemp ;
		//printf("node : %d\n",i);
	} 
}

void disp(int n)
{

	int i;
	struct Buffer* pTemp;
	printf("\n\na[0] = %u",pHashIndex[0]);
	printf("\npFL = %u\n\n",pFL);
	pTemp = pHashIndex[0] ;
	printf("in disp");
	for(i=0;i<n;i++)
	{
		printf("%u : Node : %u HQNext = %u , HQPre = %u , pFLNext = %u , pFLPre = %u\n\n",i,pTemp,pTemp->pHQNext , pTemp->pHQPrev ,pTemp->pFLNext, pTemp->pFLPrev );
		pTemp=pTemp->pHQNext;
	}
	printf("Free List\n\n ");
	pTemp = pFL;
	while(pTemp->pFLNext != NULL)
	{     
		printf("\t%u",pTemp);
		pTemp=pTemp->pFLNext;
	} 

}
