Imlement Buffer Cache : DONE 

Create virtual memory(HDD) through other processes &  which contain,  

1. Super block 
2. Inode bolck 
3. data block one block = ~100-200bytes 

We are trying to achieve following targets, 

1. Create File reguler Files and Directory.
2. OPEN FILE (if the file is directory it’ll display contents of files).
3. TRUNCATE FILE (ONLY APPLICABLE FOR REGULAR FILES).
4. APPEND FILE (ONLY APPLICABLE FOR REGULAR FILE).
5. SHOW Current Values in KERNAL DATASTRUCTURE ( Buffer Cache, Inode Cache, Incore Inode cache, etc ).
6. SHOW HDD PROPERTIES (Super Block, Inode Block and Data Block).
7. Create pipe which can read and write blocks through pipe to get feel of delayed write.

# Project Work Flow

### CREATE FILE ###

1. Ask user to enter file name or directory name and ask for permissions.	
2. Check inode block for free inode whose file type is null 
	- take 1st block from inode block and chck all 32 inodes
	- If free inode found then set refrence to next inode 
3. Put this inode into inode cache and lock this buffer 		
4. Take this inode and chnge it’s type as regular file or directory type is given by user
	- Add File name and inode number into its PARENT Directory. 
5. Check super block to get free block from data block.
6. Take 1st free block 
	- add it into buffer cache 
	- make its status busy 
7. If Regular file then write users data into buffer Cache's block data.
   If file is Directory then keep it blank.
8. If bck is getting full then write it to HDD. 
   And goto step 4.
9. When user input completed then add end of file at last of file and 
   Write it to HDD.
   
   
### READING FILE ###

1. Get inode Number of selected file from PARENT Directory.(namei)
2. Select block in which specific INODE is present. 
3. Copy that block into Buffer cache.
4. From buffer cache data block.Copy specific INODE into INODE pool or cache with some     extra info (inode number, refrence count …)
5. Read block by block from table of content through Buffer cache and print it on screen.

### TO CREATE NEW USER ###

 This feature adds into file system to get feel of user if or access permission for other user than owner, Where group id is neglected in this project.we considered all users from same group in this project.
	
1.	Creating new user means adding one regular file at 1st byte of HDD with inode 1 
 	   And file name is pass which contains username password and user-id, this pass 
	   File is already created (we can say that created byr os)
2.	When system starts program loads pass file and check authenticate user id and pass by reading that file.
3.	If authentication successful then aprpropriate user-id is assign to globle variable called user-ID.
4.	Depend on user-id user can read or write file. Depend on permissions for this user-id.

   
